@extends('layouts.app')

@section('content')
    <div class="logo">
        <div class="d-flex p-2 pl-5">
            <a href="/"><img class="img" src="/images/logo.png"></a>
        </div>
    </div>

    <div class="carousel">
        <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselIndicators" data-slide-to="1"></li>
                <li data-target="#carouselIndicators" data-slide-to="2"></li>
                <li data-target="#carouselIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/images/bannerText.png" alt="Первый слайд">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/bannerText.png" alt="Второй слайд">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/bannerText.png" alt="Третий слайд">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/images/bannerText.png" alt="4 слайд">
                </div>
            </div>
        </div>
    </div>
    <div class="site-section-1">
        <div class="container p-5">
            <div class="row">
                <div class="col-md-12 mx-auto text-center mb-5">
                    <p class="p-text">Запишись на бесплатную диагностику и тест-драйв</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <p class="s-text">Оставь свои данные, и мы перезвоним тебе, чтобы назначить индивидуальную
                        встречу:</p>
                </div>
            </div>

            <form action="/">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg d-flex justify-content-center align-items-center my-1">
                        <input type="text" id="InputEmail" placeholder="Имя">
                    </div>
                    <div class="col-lg d-flex justify-content-center align-items-center my-1">
                        <input type="text" id="InputPassword1" placeholder="Телефон">
                    </div>
                    <div class="col-lg d-flex justify-content-center align-items-center my-1">
                        <button type="submit" class="btn btn-primary btn-block">ОТПРАВИТЬ</button>
                    </div>
                </div>
            </form>

            <div class="row p-2">
                <div class="col-lg-12 d-flex justify-content-center">
                    <span class="s-text">Нажимая кнопку продолжить,вы соглашаетесь с политикой конфендициальности и даете разрешение на обработку персональных данных.  </span>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section-2">
        <div class="container">
            <div class="row">
                <div class="col text-center align-items-center">
                    <p class="p-text">Преимущества индивидуальных стелек FootBalance</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 mb-2  text-center align-items-center">
                    <div class="figure row align-items-center mx-auto mb-4 mt-3">
                        <div class="col">
                            <img src="/images/figure1.png" alt="">
                        </div>
                    </div>
                    <p class="sb-text">Бесплатная диагностика</p>
                    <p class="s16-text">стопы с помощью специального оборудования прямо в магазине</p>
                </div>

                <div class="col-md-6 col-lg-4 mb-2  text-center align-items-center">
                    <div class="figure row align-items-center mx-auto mb-4 mt-3">
                        <div class="col">
                            <img src="/images/figure2.png" alt="">
                        </div>
                    </div>
                    <p class="sb-text">Тест-драйв</p>
                    <p class="s16-text">Вы можете оценить качество и комфорт изготовленой специально для вас стельки во
                        время примерки, а затем принять решение приобрести или отказаться от нее</p>
                </div>


                <div class="col-md-6 col-lg-4 mb-2  text-center align-items-center">
                    <div class="figure row align-items-center mx-auto mb-4 mt-3">
                        <div class="col">
                            <img src="/images/figure3.png" alt="">
                        </div>
                    </div>
                    <p class="sb-text">Качество и инновации</p>
                    <p class="s16-text">Инновационный продукт, который производится по финским технологиям и гарантирует
                        наивысшее европейское качество</p>

                </div>

                <div class="col-md-6 col-lg-4 mb-2  text-center align-items-center">
                    <div class="figure row align-items-center mx-auto mb-4 mt-3">
                        <div class="col">
                            <img src="/images/figure4.png" alt="">
                        </div>
                    </div>
                    <p class="sb-text">Удобное месторасположение</p>
                    <p class="s16-text">лаборатория FootBalance расположена в ТЦ “Авиапарк”</p>
                </div>

                <div class="col-md-6 col-lg-4 mb-2  text-center align-items-center">
                    <div class="figure row align-items-center mx-auto mb-4 mt-3">
                        <div class="col">
                            <img src="/images/figure5.png" alt="">
                        </div>
                    </div>
                    <p class="sb-text">Быстрый результат</p>
                    <p class="s16-text">100% индивидуальные стельки изготавливаются всего за 10 минут прямо в магазине и
                        сканируются в 3D для повторного заказа</p>

                </div>

                <div class="col-md-6 col-lg-4 mb-5  text-center align-items-center">
                    <div class="figure row align-items-center mx-auto mb-4 mt-3">
                        <div class="col">
                            <img src="/images/figure6.png" alt="">
                        </div>
                    </div>
                    <p class="sb-text">Гарантия качества</p>
                    <p class="s16-text">Вы можете вернуть стельки в течение 10 дней, а мы вернем Вам деньги</p>

                </div>

            </div>
        </div>
    </div>
    <div class="site-section-3">
        <div class="container">
            <div class="row">
                <div class="col mt-5">
                    <div class="section-heading text-left mb-5">
                        <h2 class="p32green-text">Гарантия качества 30 дней</h2>
                    </div>
                    <p class="s16black-text">Мы производим качественный продукт, который должен удовлетворять всем</p>
                    <p class="s16black-text">потребностям покупателя. Каждому человеку должна быть предоставлена</p>
                    <p class="s16black-text">возможность испытать изготовленные индивидуально для него стельки в</p>
                    <p class="s16black-text">повседневной носке, и вернуть их в течение 10 дней в случае
                        неудовлетворенности</p>
                    <p class="s16black-text">продуктом.</p>
                    <div class="">
                        <button type="submit" class="btn btn-primary btn-block">Записаться на тестдрайв ></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section-4">
        <div class="container">
            <div class="row ">
                <div class="col ml-auto mt-5">
                    <div class="l-text">
                        <div class="section-heading text-left">
                            <p class="s32boldgreen">Индивидуальные стельки на заказ </p>
                            <p class="s32boldgreen">от 1890 руб.</p>
                        </div>
                        <div class="section-heading mb-4">
                            <p class="s16-text">На 100% индивидуальная стелька для спортивной обуви, созданная в
                                магазине, с</p>
                            <p class="s16-text">учетом особенностей Ваших ног.</p>
                            <p class="s16-text">Помня о вашем постоянном комфорте, ваши формованные стельки
                                отсканированыс</p>
                            <p class="s16-text">в 3D. Отсканированный профиль сохраняется с персональным логином,
                                поэтому вы </p>
                            <p class="s16-text">можете просто зайти на наш интернет-магазин и приобрести дополнительные
                                пары</p>
                            <p class="s16-text">персональных стелек. </p>
                        </div>
                        <div class="">
                            <button type="submit" class="btn btn-primary btn-block">Записаться на тестдрайв ></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section-5">
        <div class="container pl-2 pr-2">
            <div class="row ">
                <div class="col mb-5 text-center align-items-center">
                    <p class="s32boldgreen">Концепция FootBalance</p>
                </div>
            </div>
            <div class="row">
                <div class="col ml-3 mb-4">
                    <span class="img-rect align-middle">1</span>
                    <img class="mb-3" src="/images/conception_img1.png" alt="">
                    <p class="s16green">Диагностика</p>
                    <span>Ваши стопы анализируются с помощью специального оборудования прямо в магазине</span>
                </div>
                <div class="col ml-3  mb-4">
                    <span class="img-rect align-middle">2</span>
                    <img class="mb-3" src="/images/conception_img2.png" alt="">
                    <p class="s16green">Анализ</p>
                    <span>Снимки ваших стоп и лодыжек производятся при помощи программного обеспечения FitPro. Вы получите персональный отчет об анализе стопы по электронной почте</span>
                </div>
                <div class="col ml-3 mb-4">
                    <span class="img-rect align-middle">3</span>
                    <img class="mb-3" src="/images/conception_img3.png" alt="">
                    <p class="s16green">Изготовление</p>
                    <span>Запатентованная технология позволяет всего за 10 минут изготовить 100%индивидуальные стельки, которые идеально прилегают и повторяют все изгибы ваших стоп и поддерживают правильное положение ног</span>
                </div>
                <div class="col ml-3 mb-4">
                    <span class="img-rect align-middle">4</span>
                    <img class="mb-3" src="/images/conception_img4.png" alt="">
                    <p class="s16green">Примерка</p>
                    <span>Стельки FootBalance полностью готовы к использованию. Примеряйте и наслаждайтесь комфортом в движении</span>
                </div>
                <div class="col align-self-center ">
                    <button type="submit" class="btn btn-primary btn-block m-auto ">Записаться на тестдрайв ></button>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section-6">
        <div class="container">
            <div class="row">
                <div class="col text-center align-self-center">
                    100% ИНДИВИДУАЛЬНЫЕ СПОРТИВНЫЕ ФОРМОВАННЫЕ СТЕЛЬКИ В МАГАЗИНЕ
                </div>
                <div class="col text-center align-self-center">
                    100% ИНДИВИДУАЛЬНЫЕ ПОВСЕДНЕВНЫЕ ФОРМОВАННЫЕ СТЕЛЬКИ В МАГАЗИНЕ
                </div>
                <div class="col text-center align-self-center">
                    QUICKFIT ПЕРСОНАЛИЗИРОВАННЫЕ СТЕЛЬКИ
                </div>
                <div class="col text-center align-self-center">
                    OS1ST <br>НОСКИ, МАНЖЕТЫ, КОМПРЕССИОННОЕ БЕЛЬЕ
                </div>
            </div>
        </div>
    </div>

    <div class="site-section-7">
        <div class="container">
            <div class="row mb-2">
                <div class="col-lg-3">100% BALANCE</div>
                <div class="col-lg-6">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                               role="tab" aria-controls="nav-home" aria-selected="false">Описание</a>
                            <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                               role="tab" aria-controls="nav-profile" aria-selected="true">Характристики</a>
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
                               role="tab" aria-controls="nav-contact" aria-selected="false">Преимущества</a>
                        </div>
                    </nav>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <div class="row" style="border-top: 2px solid #dee2e6;border-bottom: 2px solid #dee2e6">
                <div class="col-lg-3">
                    <div class="d-flex pt-5 pb-5">Удобный качественный амортизатор</div>
                    <div class="d-flex pt-5 pb-5">Запантентованная формованная балансировачная
                        пластина для динамической поддержки арки стопы
                    </div>
                </div>
                <div class="col-lg-6 justify-content-center pt-3">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade" id="nav-home" role="tabpanel"
                             aria-labelledby="nav-home-tab">Описание
                        </div>
                        <div class="tab-pane fade show  active" id="nav-profile" role="tabpanel"
                             aria-labelledby="nav-profile-tab">
                            <img src="/images/sl.png" alt="">
                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                             aria-labelledby="nav-contact-tab">Преимущества
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="d-flex  pt-5 pb-5">Прочный верхний слой с антибактериальным покрытием</div>
                    <div class="d-flex  pt-5 pb-5">Высокопрочная изнанка</div>
                </div>
            </div>
        </div>
    </div>




@endsection

